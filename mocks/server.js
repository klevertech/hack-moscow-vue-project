

const Hapi = require('@hapi/hapi');


const init = async () => {
  const server = Hapi.server({
    port: 9090,
    host: 'localhost',
    routes: {
      cors: true,
    },
  });

  server.route({
    method: 'POST',
    path: '/auth',
    handler: function (request, h) {
      switch (request.payload.login) {
        case "student":
          return {
            userRole: 'student',
        };
        case "teacher": return {
          userRole: 'teacher',
        };
        case "mentor": return {
          userRole: 'mentor',
        };
        case "parent": return {
          userRole: 'parent',
        };
        default: return h.response(data).code(404)
      }
    },
  });

  server.route({
    method: 'GET',
    path: '/subjects',
    handler: (request, h) => ([
      {
        id: 1,
        name: 'Алгебра',
      },
      {
        id: 2,
        name: 'Геометрия',
      },
      {
        id: 3,
        name: 'Математика',
      },
      {
        id: 4,
        name: 'Информатика',
      },
      {
        id: 5,
        name: 'Обществознание',
      },
      {
        id: 6,
        name: 'ОБЖ',
      },
      {
        id: 7,
        name: 'Физика',
      },
      {
        id: 8,
        name: 'Химия',
      },
      {
        id: 9,
        name: 'Биология',
      },
      {
        id: 10,
        name: 'География',
      },
      {
        id: 11,
        name: 'Природоведение',
      },
      {
        id: 12,
        name: 'Окружающий мир',
      },
      {
        id: 13,
        name: 'Русский язык',
      },
      {
        id: 14,
        name: 'Литература',
      },
      {
        id: 15,
        name: 'История России',
      },
      {
        id: 16,
        name: 'Всеобщая история',
      },
      {
        id: 17,
        name: 'Английский язык',
      },
      {
        id: 18,
        name: 'Чтение',
      },
    ]),
  });

  server.route({
    method: 'GET',
    path: '/student/1',
    handler: (request, h) => ({
      id: 1,
      name: 'Фролов Афанасий Владимирович',
      grade: 9,

      marks: [4, 5, 4, 5, 3, 4, 2, 5, 4],
    }),
  });

  server.route({
    method: 'GET',
    path: '/student/card/{id}',
    handler: (request, h) => ({
      name: 'Фролов Афанасий Владимирович',
      parents: 'Фролов Аладимир Дмитриевич',
      contact: 'frolov.12@gmail.com',
      grade: 11,
      id: 1,
      image: 'https://source.unsplash.com/DnWYw0zLJBg',
      progress: {
        marks: [5, 2, 1, 2, 1, 4, 2, 3, 1],
        allTasks: 42,
        successTasks: 17,
        subjectsMarks: random(1, 5, 18),
      }
    }),
  });



  server.route({
    method: 'GET',
    path: '/students',
    handler: (request,h) => ([
      {
        name: 'Фролов Афанасий Владимирович',
        parents: 'Фролов Аладимир Дмитриевич',
        contact: 'frolov.12@gmail.com',
        grade: 11,
        id: 1,
        image: 'https://source.unsplash.com/DnWYw0zLJBg',
        marks: [5, 2, 1, 2, 1, 4, 2, 3, 1],
        allTasks: 42,
        successTasks: 17,
      },
      {
        name: 'Троянов Венеамин Максимович',
        parents: 'Троянов Максим Николаевич',
        contact: 'max.max@gmail.com',
        grade: 9,
        id: 2,
        image: 'https://zooclub.ru/attach/fotogal/clip/ezh/15.jpg',
        marks: [5, 2, 1, 2, 4, 2, 3],
        allTasks: 18,
        successTasks: 16,
      },
      {
        name: 'Раскольникова Татьяна Валерьевна',
        parents: 'Раскольников Виктор Иванович',
        contact: 'Vector.Rask@gmail.com',
        grade: 6,
        id: 3,
        image: 'https://картинки-для-срисовки.рф/image/cache/catalog/vse-kartinki/kotiki/kotik-006-400x400.jpg',
        marks: [5, 5, 5, 5, 4, 2, 3, 1],
        allTasks: 111,
        successTasks: 11,
      },
      {
        name: 'Мальков Тарас Генадиевич',
        parents: 'Мальков Генадий Максимович',
        contact: 'malkov673@gmail.com',
        grade: 6,
        id: 4,
        image: 'https://zooclub.ru/attach/fotogal/clip/ezh/15.jpg',
        marks: [5, 5, 5, 5, 4, 2, 3, 1],
        allTasks: 111,
        successTasks: 11,
      },
      {
        name: 'Шкваркин Игорь Николаевич',
        parents: 'Шкваркин Нкиолай Игоревич',
        contact: 'nick_1989@gmail.com',
        grade: 6,
        id: 5,
        image: 'https://картинки-для-срисовки.рф/image/cache/catalog/vse-kartinki/kotiki/kotik-006-400x400.jpg',
        marks: [5, 5, 5, 5, 4, 2, 3, 1],
        allTasks: 111,
        successTasks: 11,
      },
      {
        name: 'Трофимова Валерия Петровна',
        parents: 'Трофимов Петр Максимович',
        contact: 'trofim_ov@gmail.com',
        grade: 6,
        id: 6,
        image: 'https://zooclub.ru/attach/fotogal/clip/ezh/15.jpg',
        marks: [5, 5, 5, 5, 4, 2, 3, 1],
        allTasks: 111,
        successTasks: 11,
      },
      {
        name: 'Колесников Константин Валентинович',
        parents: 'Колесников Валентин Александрович',
        contact: 'kolesnick@gmail.com',
        grade: 6,
        id: 7,
        image: 'https://картинки-для-срисовки.рф/image/cache/catalog/vse-kartinki/kotiki/kotik-006-400x400.jpg',
        marks: [5, 5, 5, 5, 4, 2, 3, 1],
        allTasks: 111,
        successTasks: 11,
      },
    ]),
  });

  server.route({
    method: 'GET',
    path: '/topics',
    handler: (request, h) => ({
      algebra: [
        {
          id: 1,
          name: 'Квадратные уравнения',
        },
        {
          id: 2,
          name: 'Решение квадратных уравнений',
        },
      ],
      geometry: [
        {
          id: 1,
          name: 'Теорема Пифагора',
        },
        {
          id: 2,
          name: 'Теорема равенства треугольников',
        },
      ],
    }),
  });

  server.route({
    method: 'GET',
    path: '/works',
    handler: (request, h) => ({
      homeWork: [
        {
          id: 1,
          status: 'Выполнена',
          author: 'Трофимова Валерия Петровна',
          subject: 'Геометрия',
          description: 'lorem ipsum, home work geometry by Trofimova Valery',
        },
        {
          id: 2,
          status: 'Отклонена',
          author: 'Колесников Константин Валентинович',
          subject: 'Алгебра',
          description: 'lorem ipsum, home work algebra by Kolesnikov Kostya',
        },
        {
          id: 3,
          status: 'Не проверена',
          author: 'Мальков Тарас Генадиевич',
          subject: 'Математика',
          description: 'lorem ipsum, home work math by Malkov Taras',
        },

      ],
    }),
  });


  await server.start();
  console.log('Server is running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();

function random(min, max, number) {
  let arr = [];
  for (let i = 0; i < number; i += 1) {
    arr.push(Math.floor(Math.random() * (max - min + 1) + min));
  }
  return arr;
}
