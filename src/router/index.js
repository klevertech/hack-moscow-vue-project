import Vue from 'vue';
import VueRouter from 'vue-router';
import routerNames from './routeNames';

import StudentCard from '../pages/StudentCard.vue';
import StudentList from '../pages/StudentList.vue';
import Login from '../pages/Login.vue';
import Mentor from '../pages/Mentor.vue';
import Teacher from '../pages/Teacher.vue';
import Student from '../pages/Student.vue';
import HomeworksPage from '../pages/HomeworksPage.vue';
import HomeworkPage from '../pages/HomeworkPage.vue';
// import Parent from '../pages/Parent.vue';

import Parent from '../pages/Parent/Parent.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      name: routerNames.login,
      component: Login,
    },
    {
      path: '/mentor',
      name: routerNames.mentor,
      component: Mentor,
    },
    {
      path: '/teacher',
      name: routerNames.teacher,
      component: Teacher,
    },
    {
      path: '/student',
      name: routerNames.student,
      component: Student,
    },
    {
      path: '/parent',
      name: routerNames.parent,
      component: Parent,
    },
    {
      path: '/students',
      component: StudentList,
    },
    {
      path: '/student/card/:id',
      component: StudentCard,
    },
    {
      path: '/homeworks-page',
      component: HomeworksPage,
    },
    {
      path: '/homework-page',
      name: routerNames.homeworkPage,
      component: HomeworkPage,
    },
  ],
});

export default router;
