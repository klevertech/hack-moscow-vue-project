export default {
  login: 'Login',
  parent: 'Parent',
  student: 'Student',
  teacher: 'Teacher',
  mentor: 'Mentor',
  studentInfo: 'StudentInfo',
  studentCard: 'StudentCard',
  studentList: 'StudentList',
  homeworksPage: 'HomeworksPage',
  homeworkPage: 'HomeworkPage',
};
