import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userRole: 'mentor',
  },
  getters: {
  },
  mutations: {
    setRole: (state, val) => {
      state.userRole = val;
    },
  },
  actions: {
  },
  modules: {
  },
});
