export default {
  list: [
    {
      text: 'Календарь',
      url: '/calendar',
      iconName: 'el-icon-date',
      id: 1,
    },
    {
      text: 'Домашнее задание',
      url: '/homeworks-page',
      iconName: 'el-icon-collection',
      id: 2,
    },
    {
      text: 'Успеваемость',
      url: '/academic-performance',
      iconName: 'el-icon-finished',
      id: 3,
    },
    {
      text: 'Библиотека и материалы',
      url: '/library',
      iconName: 'el-icon-reading',
      id: 4,
    },
  ],
};
