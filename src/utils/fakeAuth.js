const teacher = (login = '', password = '') => {
  const fakeTeacher = login === 'teacher' && password === 'teacher';

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fakeTeacher) {
        resolve();
      } else {
        reject();
      }
    }, 1000);
  });
};
const student = (login = '', password = '') => {
  const fakeTeacher = login === 'student' && password === 'student';

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fakeTeacher) {
        resolve();
      } else {
        reject();
      }
    }, 1000);
  });
};
const parent = (login = '', password = '') => {
  const fakeTeacher = login === 'parent' && password === 'parent';

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fakeTeacher) {
        resolve();
      } else {
        reject();
      }
    }, 1000);
  });
};
const mentor = (login = '', password = '') => {
  const fakeTeacher = login === 'mentor' && password === 'mentor';

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (fakeTeacher) {
        resolve();
      } else {
        reject();
      }
    }, 1000);
  });
};
export {
  teacher,
  student,
  parent,
  mentor,
};
